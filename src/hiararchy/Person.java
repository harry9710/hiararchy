package hiararchy;

public class Person {
	private String name, surname;
	private int age, height;
	private boolean isDead;
	private final int MIN_AGE = 1;

	//constructor
	public Person(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	//overloading constructor
	public Person(String name, String surname, int age, int height, boolean isDead) {
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.height = height;
		this.isDead = isDead;
		if (age < MIN_AGE) {
			System.out.println("Please input an already-born person!");
		}
	}

	//method printing out the name
	public void displayName() {
		System.out.println(surname + " " + name);
	}
}
