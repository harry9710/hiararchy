/* Create a class hiararchy
+ Person: name, surname, age +2 additinal attributes
+ Classes extending, teacher, student
+ Teacher, student, at least 1 attribute
+ Person should have attributes common for all 
+ There should be a common method displayName()
for student it should return students name, for teacher it should return name with the title, for example professor
+ Create another class with main method to test the person class, use your own name */

package hiararchy;

public class Main {

	public static void main(String[] args) {
		//test overloading constructor of Person class
		Person aPerson = new Person("NotBorn", "IAm", -1, 69, true); //parameters: name, surname, age, height, isDead
		aPerson.displayName();
		
		//test Student class
		Student aStudent = new Student("Hieu", "Trong", 15943); //parameters: name, surname, id
		aStudent.displayName();
		
		//test Teacher class
		Teacher aTeacher = new Teacher("Derda", "Adrian", "Professor"); //parameters: name, surname, tittle
		aTeacher.displayName();
	}
}
