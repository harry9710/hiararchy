package hiararchy;

public class Student extends Person {
	private int id;

    //constructor
	public Student(String name, String surname, int id) {
		super(name, surname); //call the first constructor of Person class
		this.id = id;
	}
    
    //overriding method
	public void displayName() {
		System.out.print("ID: " + id + " - Student: ");
		super.displayName(); //call displayName method of Person class
	}
}
