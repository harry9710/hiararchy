package hiararchy;

public class Teacher extends Person {
	private String tittle;

    //constructor
	public Teacher(String name, String surname, String tittle) {
		super(name, surname); //call the first constructor of Person class
		this.tittle = tittle;
	}
    
    //overriding method
	public void displayName() {
		System.out.print(tittle + ": ");
		super.displayName(); //call displayName method of Person class
	}
}
